from PyQt5.QtWidgets import QLabel, QWidget, QSizePolicy, QGraphicsDropShadowEffect 
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import QEvent, QPropertyAnimation, QSize, QRect, QPoint, Qt,\
                         QEasingCurve

import time

class PingPongAnim:

    def __init__(self, object, property, start, end, duration,
                    curve=QEasingCurve.Linear):
        self.property=getattr(object,property.decode("utf-8"))
        self.ping_anim=QPropertyAnimation(object, property)
        self.pong_anim=QPropertyAnimation(object, property)
        self.ping_anim.setEasingCurve(curve)
        self.pong_anim.setEasingCurve(curve)
        self.ping_anim.setDuration(duration)
        self.pong_anim.setDuration(duration)
        self.timer=time.perf_counter()
        self.duration=duration
        self.object=object
        self.start=start
        self.end=end

    def ping(self):
        left = min(time.perf_counter()-self.timer, self.duration)
        self.pong_anim.stop()
        self.ping_anim.setDuration(left*1000)
        self.ping_anim.setStartValue(self.property())
        self.ping_anim.start()
    
    def pong(self):
        left = min(time.perf_counter()-self.timer, self.duration)
        self.ping_anim.stop()
        self.pong_anim.setDuration(left*1000)
        self.pong_anim.setStartValue(self.property())
        self.pong_anim.start()

    def setStartValue(self, value):
        self.ping_anim.setStartValue(value)
        self.pong_anim.setEndValue(value)
    
    def setEndValue(self, value):
        self.ping_anim.setEndValue(value)
        self.pong_anim.setStartValue(value)