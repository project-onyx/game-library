from PyQt5.QtCore import QPoint, QRect, QSize, Qt, pyqtSignal
from PyQt5.QtWidgets import QLayout, QSizePolicy, QWidget, QGridLayout, QHBoxLayout,\
                             QWidget, QFrame, QLabel, QOpenGLWidget
from PyQt5.QtGui import QPalette, QColor


class LibraryView(QFrame):
    def __init__(self, name):
        super().__init__()
        self.name = name

    def enable(self):
        pass

    def disable(self):
        pass

class LibraryViewGL(QOpenGLWidget):
    def __init__(self, name):
        super().__init__()
        self.name = name

    def enable(self):
        pass

    def disable(self):
        pass