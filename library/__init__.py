from PyQt5.QtCore import QPoint, QRect, QSize, Qt, pyqtSignal, QPropertyAnimation, \
    QEasingCurve
from PyQt5.QtWidgets import QLayout, QSizePolicy, QWidget, QGridLayout, QHBoxLayout,\
    QVBoxLayout, QScrollArea, QFrame, QGraphicsDropShadowEffect, QGraphicsOpacityEffect
from PyQt5.QtGui import QPalette, QLinearGradient, QColor

from .games.tileview import TileView
from .store.browser import Browser
from .video.theater import Theater
from .topbar import TopBar
from GameOS.gameos_utils import EasingGradiant

class TopBarShadow(QFrame):
    def __init__(self, parent):
        super().__init__(parent)
        gradiant=EasingGradiant(self.geometry().topLeft(), QPoint(0,20),
                                QColor(0,0,0,255), QColor(0,0,0,0))
        # gradiant=QLinearGradient(self.geometry().topLeft(), QPoint(0,20))
        # gradiant.setColorAt(0.0, Qt.black)
        # gradiant.setColorAt(1.0, Qt.transparent)
        effect = QGraphicsOpacityEffect(self)
        effect.setOpacityMask(gradiant)
        self.setGraphicsEffect(effect)

class LibScroll(QScrollArea):

    scroll_signal = pyqtSignal(int)

    def __init__(self, parent):
        super().__init__(parent)

        self.verticalScrollBar().setEnabled(False)
        self.horizontalScrollBar().setEnabled(False)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setFrameShape(QFrame.NoFrame)
        self.scroll_anim = QPropertyAnimation(self.horizontalScrollBar(), b"value")
        self.scroll_anim.setEasingCurve(QEasingCurve.OutCubic)
        # self.scroll_signal.connect(self.scroll_to)
        self.widget = QWidget(self)

        pal = QPalette(Qt.transparent)
        self.widget.setPalette(pal)
        self.setPalette(pal)
        self.setWidget(self.widget)
        self.setAutoFillBackground(False)
        self.widget.setAutoFillBackground(False)

        self.view_layout = QHBoxLayout(self.widget)
        self.view_layout.setContentsMargins(0,0,0,0)
        self.view_layout.setSpacing(0)
        # self.setWidgetResizable(False)

        self.shadow = TopBarShadow(self)
    
    def resizeEvent(self, event):
        super().resizeEvent(event)
        self.shadow.resize(self.width(), 30)

    def addView(self, widget):
        widget.setParent(self.widget)
        self.view_layout.addWidget(widget)

    def scroll_to(self, widget):
        scroll=self.horizontalScrollBar().value()
        diff = abs(scroll-widget.x())
        anim_time=800
        if diff < self.width():
            anim_time = anim_time*diff / self.width()
        self.scroll_anim.stop()
        self.scroll_anim.setStartValue(scroll)
        self.scroll_anim.setEndValue(widget.x())
        self.scroll_anim.setDuration(anim_time)
        self.scroll_anim.start()

class Library(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        layout=QVBoxLayout(self)
        layout.setContentsMargins(0,0,0,0)
        self.top_bar = TopBar(self)
        self.side_scroll = LibScroll(self)
        layout.addWidget(self.top_bar)
        layout.addWidget(self.side_scroll)

        games = TileView("GAMES")
        store = Browser("STORE")
        video = Theater("VIDEO")

        self.addView(games)
        self.addView(store)
        self.addView(video)

    def enable(self):
        self.top_bar.tabs.enable()

    def disable(self):
        self.top_bar.tabs.disable()
    
    def addView(self, view):
        self.top_bar.tabs.addTab(view.name, view)
        self.top_bar.tabs.sele_sig.connect(self.side_scroll.scroll_to)
        self.side_scroll.addView(view)
    
    def resizeEvent(self,event):
        super().resizeEvent(event)
        height = self.side_scroll.height()
        self.side_scroll.widget.resize(self.width()*3, height)
        layout=self.side_scroll.view_layout
        for i in range(layout.count()):
            widget = layout.itemAt(i).widget()
            widget.resize(self.width(),widget.height())
