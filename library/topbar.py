from PyQt5.QtCore import QPoint, QRect, QSize, Qt, pyqtSignal
from PyQt5.QtWidgets import QLayout, QSizePolicy, QWidget, QGridLayout, QHBoxLayout, \
                             QWidget, QFrame, QLabel, QGraphicsDropShadowEffect
from PyQt5.QtGui import QPalette, QColor
from utils import stylize
from GameOS import config
from ..common import gamepad
                
class Tab(QLabel):
    def __init__(self, parent, name, view):
        super().__init__(parent)
        # self.setObjectName(id)
        self.setText(name)
        self.setAlignment(Qt.AlignCenter)
        self.view = view
        
    def mousePressEvent(self, event):
        self.parent().select(self)
    
class LibraryTabs(QFrame):

    next_sig = pyqtSignal()
    prev_sig = pyqtSignal()
    sele_sig = pyqtSignal(QWidget)

    def __init__(self, parent):
        super().__init__(parent)
        self.setObjectName("library_tabs")
        self.setFixedSize(400, 36)
        layout = QHBoxLayout()

        # game_tab = Tab(self, "GAMES", "library_game_tab")
        # store_tab = Tab(self, "STORE", "library_store_tab")
        # video_tab = Tab(self, "VIDEO", "library_video_tab")

        layout.setContentsMargins(2,2,2,2)
        layout.setSpacing(2)

        self.next_sig.connect(self.select_next)
        self.prev_sig.connect(self.select_prev)
        self.selected = None
        self.setLayout(layout)
    
    def addTab(self, name, view):
        layout = self.layout()
        tab = Tab(self, name, view)
        num = layout.count()
        if not num:
            tab.setProperty("first", True)
        else:
            widget=layout.itemAt(num-1).widget()
            widget.setProperty("last", False)
            widget.setStyle(widget.style())
        tab.setProperty("last", True)
        tab.setProperty("selected", False)
        tab.setStyle(tab.style())
        layout.addWidget(tab)
        return tab
    
    def select_next(self):
        for i in range(self.layout().count()-1):
            if self.layout().itemAt(i).widget() is self.selected:
                self.select(self.layout().itemAt(i+1).widget())
                break
    
    def select_prev(self):
        for i in range(self.layout().count()-1,0,-1):
            if self.layout().itemAt(i).widget() is self.selected:
                self.select(self.layout().itemAt(i-1).widget())
                break
        
    def select(self, item):
        if not self.selected == None:
            self.selected.setProperty("selected", False)
            self.selected.setStyle(self.selected.style())
            self.selected.view.disable()
            self.sele_sig.emit(item.view)
        item.setProperty("selected", True)
        item.setStyle(item.style())
        item.view.enable()
        self.selected = item

    def _sel_next_cb(self, code, state):
        self.next_sig.emit()

    def _sel_prev_cb(self, code, state):
        self.prev_sig.emit()
    
    def enable(self):
        manager.register_callback(self._sel_next_cb, BTN_TR=1)
        manager.register_callback(self._sel_prev_cb, BTN_TL=1)
        if not self.selected == None:
            self.selected.view.enable()
        else:
            self.select(self.layout().itemAt(0).widget())
    
    def disable(self):
        manager.unregister_callback(self._sel_next_cb, "BTN_TR", 1)
        manager.unregister_callback(self._sel_prev_cb, "BTN_TL", 1)
        if not self.selected == None:
            self.selected.view.disable()
        
class TopBar(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.tabs = LibraryTabs(self)
        self.setFixedHeight(50)


    def resizeEvent(self,event):
        super().resizeEvent(event)
        self.tabs.move(self.width()/2 - self.tabs.width()/2, 12)