from PyQt5.QtWidgets import QWidget, QOpenGLWidget, QGraphicsOpacityEffect
from PyQt5.QtGui import QImage, QOpenGLTextureBlitter, QOpenGLTexture, QPainter, \
    QOpenGLContext, QColor, QRadialGradient, QBrush, QLinearGradient, QOpenGLShader,\
    QOpenGLShaderProgram, QVector3D, QVector2D
from PyQt5.QtCore import Qt, QEvent, QSize, QRect, QRectF, QPointF, QPoint, \
    pyqtSignal
from PyQt5.QtMultimedia import QSound

from GameOS import config
from GameOS.gameos_utils import EasingRadialGradiant, PrimitiveDisc, transform_2d
from GameOS.gamemanager import GameManagerClient

from .scrollarea import ScrollArea
from ..library import LibraryViewGL
from ...common import gamepad

import OpenGL.GL as gl
import OpenGL.GLU as glu

class GameView(LibraryViewGL):

    paint_shadow = pyqtSignal(QWidget)
    paint_buffer = pyqtSignal()

    def __init__(self, name):
        super().__init__(name)
        self.shadow_radius = 40
        self.scroll_area = ScrollArea(self)
        self._layout=self.scroll_area.grid_layout
        self.y_offset=config.header.shadow_offset
        self.paint_shadow.connect(self.paintShadow)
        self.paint_buffer.connect(self.paintGL)
        
        self.gamemanager = GameManagerClient()
        
        # self.topshadow.setGeometry(0,0,self.geometry().width(),30)
        # self.topshadow.set
        # self.topshadow.show()

        self.focus_sound = QSound(":/sound/focus")
        
        self.painter = QPainter()   
        self.gradient = EasingRadialGradiant(QColor(100,120,140),
                                            QColor(11,13,20), step=14)

        if self._layout.count():
            self._layout.selected.widget().select()
    
    def initializeGL(self):
        self.shadow_img=QImage(":/image/shadow")
        self.texture = QOpenGLTexture(self.shadow_img)
        self.texture.setMinificationFilter(QOpenGLTexture.LinearMipMapLinear)
        self.blitter = QOpenGLTextureBlitter()
        self.blitter.create()
        self.shader = QOpenGLShaderProgram()
        self.shader.addCacheableShaderFromSourceFile(
            QOpenGLShader.Fragment, ":/shader/background_light")
        self.shader.addCacheableShaderFromSourceFile(
            QOpenGLShader.Vertex, ":/shader/tranform_2d")
        self.shader.link()
        self.shader.bind()
        self.shader.setUniformValue(2, QVector3D(0.04,0.05,0.08))
        self.shader.setUniformValue(3, QVector3D(0.4,0.47,0.55))

        self.disk = PrimitiveDisc(0.5)
    
    def paintGL(self):

        gl.glClearColor(0.04,0.05,0.08,1)

        area = QRect(QPoint(0,0),self.size())
        point=QPoint(self.width()/2,-700)
        radius=max(self.width(),self.height())
        self.shader.bind()
        matrix = transform_2d(point.x(), point.y(), radius, radius,
                                self.width(), self.height())
        self.shader.setUniformValue(1, matrix)
        self.disk.draw()

        # self.painter.begin(self)
        # self.gradient.setCenter(point)
        # self.gradient.setFocalPoint(point)
        # self.gradient.setRadius(radius)
        # self.painter.fillRect(area, self.gradient)
        # self.painter.end()

        self.blitter.bind()
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_DST_COLOR, gl.GL_ZERO)
        for widget in self.listShadows():
            self.paintShadow(widget)
        gl.glDisable(gl.GL_BLEND)
        self.blitter.release()

        # self.painter.end()
        # print("ok")
        # self.painter.endNativePainting()
        # print("ko")
    def listShadows(self):
        grid = self.scroll_area.grid_layout
        if grid.count():
            sep = grid.spacing()
            scroll = self.scroll_area.verticalScrollBar().value()
            top = max(0, scroll - self.shadow_radius)
            bottom = scroll + self.height() + self.shadow_radius - sep
            height = grid.itemList[0].widget().sizeHint().height() + sep
            start_id = top//height * grid.num_column
            end_id=(bottom//height+1)*grid.num_column

            for i in range(max(0,start_id), min(grid.count(),end_id)):
                yield grid.itemList[i].widget()

    def paintShadow(self, widget):
        scroll = self.scroll_area.verticalScrollBar().value()
        offset = QPoint(0,scroll-self.y_offset)
        rect = QRect(widget.pos() - offset, widget.size())
        margin = QPoint(self.shadow_radius,self.shadow_radius)
        bot_ri = rect.bottomRight()
        geometry = QRectF(rect.topLeft()-margin, bot_ri+margin)
        area = QRect(QPoint(0,0),self.size())
        target = QOpenGLTextureBlitter.targetTransform(geometry,area)
        self.blitter.blit(self.texture.textureId(), target,
                    QOpenGLTextureBlitter.OriginTopLeft)
        
    def resizeEvent(self,event):
        super().resizeEvent(event)
        self.scroll_area.resize(self.size())

    def enable(self):
        manager.register_callback(self._layout.get_stick_zone, "ABS_X", "ABS_Y")
        manager.register_callback(self._layout.move_left,   ABS_HAT0X=-1)
        manager.register_callback(self._layout.move_right,  ABS_HAT0X= 1)
        manager.register_callback(self._layout.move_up,     ABS_HAT0Y=-1)
        manager.register_callback(self._layout.move_down,   ABS_HAT0Y= 1)
        manager.register_callback(self._layout.enter,       BTN_SOUTH= 1)


    def disable(self):
        manager.unregister_callback(self._layout.get_stick_zone, "ABS_X", 0)
        manager.unregister_callback(self._layout.get_stick_zone, "ABS_Y", 0)
        manager.unregister_callback(self._layout.move_left,   "ABS_HAT0X",-1)
        manager.unregister_callback(self._layout.move_right,  "ABS_HAT0X", 1)
        manager.unregister_callback(self._layout.move_up,     "ABS_HAT0Y",-1)
        manager.unregister_callback(self._layout.move_down,   "ABS_HAT0Y", 1)
        manager.unregister_callback(self._layout.enter,       "BTN_SOUTH", 1)