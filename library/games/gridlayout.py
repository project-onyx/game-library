from PyQt5.QtCore import QPoint, QRect, QSize, QPointF
from PyQt5.QtWidgets import QLayout, QSizePolicy
from PyQt5.QtGui import QEnterEvent, QDragLeaveEvent, QPixmap
from GameOS.flowlayout import FlowLayout
from GameOS import common
from GameOS import config
from .gameheader import GameHeader

from steamutils import *
from utils import trigger_on, register
import math
import os

UP          = 0
UP_RIGHT    = 1
RIGHT       = 2
DOWN_RIGHT  = 3
DOWN        = 4
DOWN_LEFT   = 5
LEFT        = 6
UP_LEFT     = 7

class GridLayout(FlowLayout):
    
    def __init__(self, parent=None, margin=0, spacing=-1):
        super().__init__(parent, margin, spacing)
        self.num_column=1
        self.scrollarea=parent.parent()
        self.default_img = QPixmap(":/image/default_header")
        self.sep_width=common.properties["min_spacing"]
        self.tile_scale=0.7
        self.prev_zone = None
        self.stick_flag = False
        self.load_headers()
        if self.count():
            self.selected=self.itemList[0]
        else:
            self.selected=None
        common.app.mouse_signal.connect(self.hide_last_header)
    
    def __new__(cls,*args, **kwargs):
        cls.directions = dict()
        for method in vars(cls).values():
            id = getattr(method, "_id", None)
            if not id ==None:
                cls.directions[id] = method
        return super().__new__(cls,*args, **kwargs)
    
    def load_headers(self):
        for game in sorted(self.steam_manager.games,
                            key=lambda x: x.name.lower()):
            header = GameHeader(self, game, self.tile_scale)
            self.addWidget(header)
        # for i,file in enumerate(os.listdir(self.gridpath)):
        #     fullpath=os.path.join(config.path.gridpath, file)
        #     header = GameHeader(self, "", fullpath, self.tile_scale)
        #     self.addWidget(header)

    def get_stick_zone(self, code, state):
        x=manager.get_axis("ABS_X")
        y=manager.get_axis("ABS_Y")
        deadzone=config.core.deadzone

        if math.hypot(x,y) < deadzone:
            self.prev_zone = None
        else:
            angle = math.atan2(x,y)/(math.pi*0.25)
            angle += 0.5
            if angle < 0: angle+=8
            zone_id = int(angle)
            if zone_id != self.prev_zone and not zone_id & 1:
                self.__class__.directions[zone_id](self)
                self.prev_zone = zone_id

    def enter(self, code, state):
        self.selected.widget().run_signal.emit()

    def adjust_scroll(self):
        selected=self.selected.widget()
        s_value  = self.scrollarea.verticalScrollBar().value()
        w_top    = selected.y() - self.spacing()
        w_bottom = selected.y() + self.spacing() + selected.height()
        v_top    = w_bottom - self.scrollarea.height()
        v_bottom = s_value + self.scrollarea.height()
        if w_top < s_value:
            self.scrollarea.scroll_signal.emit(w_top)
        if w_bottom > v_bottom:
            self.scrollarea.scroll_signal.emit(v_top)
    
    def hide_last_header(self):
        if self.selected:
            self.selected.widget().leave_signal.emit()
    
    def select(self, widget):
        for item in self.itemList:
            if item.widget() is widget:
                self.selected = item
                self.adjust_scroll()
                return True
        return False
    
    @register(UP)
    def move_up(self,code=None,state=None):
        if self.count():
            id=self.itemList.index(self.selected)
            if id >= self.num_column:
                self.selected.widget().leave_signal.emit()
                widget = self.itemList[id-self.num_column]
                widget.widget().enter_signal.emit()

    @register(DOWN)
    def move_down(self,code=None,state=None):
        if self.count():
            id=self.itemList.index(self.selected)
            if id + self.num_column < self.count():
                self.selected.widget().leave_signal.emit()
                widget = self.itemList[id+self.num_column]
                widget.widget().enter_signal.emit()

    @register(LEFT)
    def move_left(self,code=None,state=None):
        if self.count():
            id=self.itemList.index(self.selected)
            if id % self.num_column:
                self.selected.widget().leave_signal.emit()
                widget = self.itemList[id-1]
                widget.widget().enter_signal.emit()
    
    @register(RIGHT)
    def move_right(self,code=None,state=None):
        if self.count():
            id=self.itemList.index(self.selected)
            if (id % self.num_column) < self.num_column-1 and id < self.count()-1:
                self.selected.widget().leave_signal.emit()
                widget = self.itemList[id+1]
                widget.widget().enter_signal.emit()

    def doLayout(self, rect, testOnly):
        x = rect.x()
        y = rect.y()
        lineHeight = 0
        if self.count():
            space = self.spacing()
            size_hint = self.itemList[0].widget().sizeHint()
            item_width = size_hint.width()
            item_height = size_hint.height()
            self.num_column = (rect.width() - space) // (item_width + space)
            if self.num_column > 0:
                room = rect.width() - self.num_column*item_width
                self.sep_width = room / (self.num_column+1)
                y += space
                x += self.sep_width
                for i, item in enumerate(self.itemList):
                    if i and i % self.num_column == 0:
                        x = rect.x() + self.sep_width
                        y += item_height + space
                    item.widget().setGeometry(QRect(QPoint(x, y), item.widget().sizeHint()))
                    x += item_width + self.sep_width
                y += item_height + space
        return y - rect.y()

# def move_filter(self, value, code):
#     x=manager.get_axis("ABS_X")
#     y=manager.get_axis("ABS_Y")
#     return abs(x) > 0.5 or abs(y) > 0.5

    # @trigger_on(move_filter)
    # def joystick_move(self, value, code):
    #     if code == "ABS_X":
    #         if value > 0.5: self.move_right()
    #         elif value < -0.5: self.move_left()
    #     elif code == "ABS_Y":
    #         if value > 0.5: self.move_up()
    #         elif value < -0.5: self.move_down()

        # elif self.prev_zone == None:
        #     zone_id = get_direction(8) 
        #     self.stick_flag = zone_id
            # self.__class__.directions[zone_id](self)

            # if self.stick_flag & 0x1:
                # print(zone_id)
                # if zone_id in ((self.stick_flag+3)&7, (self.stick_flag-3)&7):
                #     self.__class__.directions[zone_id](self)

        #     if diag:
        #         if self.prev_zone == None:
        #             self.__class__.directions[zone_id](self)
        #     else:
        #         if self.prev_zone == None:
        #             self.__class__.directions[zone_id](self)
        #         elif not self.prev_zone & 0x1:
        #             self.__class__.directions[zone_id](self)
        # self.prev_zone = zone_id
            # if zone_id & 0x1:
            #     self.stick_flag=False
            #     if not self.prev_zone == None:
            #         self.prev_zone = zone_id
            #         print("no")
            #         return
            # if not zone_id == self.prev_zone:
            #     self.prev_zone = zone_id
            #     self.__class__.directions[zone_id](self)

    # @register(UP_RIGHT)
    # def move_up_right(self,code=None,state=None):
    #     if self.count():
    #         id=self.itemList.index(self.selected)
    #         if self.num_column > 1 and id >= self.num_column and \
    #             id % self.num_column < self.num_column-1:
    #             self.selected.widget().leave_signal.emit()
    #             widget = self.itemList[id-self.num_column+1]
    #             widget.widget().enter_signal.emit()
            
    # @register(DOWN_RIGHT)
    # def move_down_right(self,code=None,state=None):
    #     if self.count():
    #         id=self.itemList.index(self.selected)
    #         if self.num_column > 1 and self.count()-id > self.num_column+1 and \
    #             id % self.num_column < self.num_column-1:
    #             self.selected.widget().leave_signal.emit()
    #             widget = self.itemList[id+self.num_column+1]
    #             widget.widget().enter_signal.emit()
        
    # @register(DOWN_LEFT)
    # def move_down_left(self,code=None,state=None):
    #     if self.count():
    #         id=self.itemList.index(self.selected)
    #         if self.num_column > 1 and id % self.num_column and \
    #             self.count()-id > self.num_column-1:
    #             self.selected.widget().leave_signal.emit()
    #             widget = self.itemList[id+self.num_column-1]
    #             widget.widget().enter_signal.emit()
    
    # @register(UP_LEFT)
    # def move_up_left(self,code=None,state=None):
    #     if self.count():
    #         id=self.itemList.index(self.selected)
    #         if self.num_column > 1 and id > self.num_column and id % self.num_column:
    #             self.selected.widget().leave_signal.emit()
    #             widget = self.itemList[id-self.num_column-1]
    #             widget.widget().enter_signal.emit()