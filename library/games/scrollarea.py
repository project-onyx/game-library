from PyQt5.QtCore import QPoint, QRect, QSize, QPointF, pyqtSlot, pyqtSignal, Qt, \
                            QPropertyAnimation, QEvent, QEasingCurve, pyqtProperty
from PyQt5.QtWidgets import QLayout, QSizePolicy, QWidget, QScrollArea, QFrame
from PyQt5.QtGui import QEnterEvent, QDragLeaveEvent, QPalette
from GameOS import common
from GameOS import config
from ...common import gamepad
from .gameheader import GameHeader
from .gridlayout import GridLayout

from utils import lerp, stylize

from threading import Thread, RLock, Event
import time

class Helper(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.show()

    def paintEvent(self, event):
        self.parent().parent().update()

class ScrollArea(QScrollArea):

    scroll_signal = pyqtSignal(int)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.horizontalScrollBar().setEnabled(False)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setFrameShape(QFrame.NoFrame)
        self.setWidgetResizable(True)
        self.scroll_anim = QPropertyAnimation(self.verticalScrollBar(), b"value")
        self.scroll_anim.setEasingCurve(QEasingCurve.OutCubic)
        # self.scroll_anim = Animation(self.verticalScrollBar(),"setValue")
        min_sep=common.properties["min_spacing"]
        self.scroll_signal.connect(self.scroll_to)
        self.widget = QWidget(self)

        pal = QPalette(Qt.transparent)
        self.widget.setPalette(pal)
        self.setPalette(pal)
        
        self.grid_layout = GridLayout(self.widget, min_sep, min_sep)
        self.setWidget(self.widget)
        self.setAutoFillBackground(False)
        self.widget.setAutoFillBackground(False)
        # self.helper = Helper(self)

    def paintEvent(self, event):
        self.parent().update()

    # def eventFilter(self, object, event):
    #     if event.type() == QEvent.Paint:
    #         # backup = self.eventFilter
    #         # self.eventFilter = self.pain_fallback
    #         print(type(self.viewport()))#.update()
    #         # self.eventFilter = backup
    #         return False
    #     return True

    def scroll_to(self, value):
        if self.grid_layout.count():
            height=self.grid_layout.itemList[0].widget().height()
            height+=self.grid_layout.sep_width
            diff = abs(self.verticalScrollBar().value() - value)
            anim_time=400
            if diff < height:
                anim_time = anim_time*diff / height
            self.scroll_anim.stop()
            self.scroll_anim.setStartValue(self.verticalScrollBar().value())
            self.scroll_anim.setEndValue(value)
            self.scroll_anim.setDuration(anim_time)
            self.scroll_anim.start()

class Animation:

    def __init__(self, object, anim, start=None, end=None, duration=0.1, step=1/60):
        self._thread = Thread(target=self.run)
        self._stop_event=Event()
        self._start_event=Event()
        self._complet_event=Event()
        self._value=start
        self.duration=duration
        self.start_value=start
        self.end_value=end
        self.step=step
        self.property=getattr(object, anim)
        self._lock=RLock()

    def run(self):
        # while self._start_event.wait():
        #     self._start_event.clear()
        self._stop_event.clear()
        start=last=time.clock()
        while not(time.perf_counter()-start > self.duration
                    or self._stop_event.is_set()):
            deltatime = time.perf_counter() - last
            last = time.perf_counter()
            if deltatime < self.step:
                time.sleep(self.step-deltatime)
            with self._lock:
                bias=min((time.perf_counter()-start)/self.duration,1)
                self._value = lerp(self.end_value, self.start_value, bias)               
            self.property(self._value)
        self._complet_event.set()
            
    def start(self):
        if not self._thread.is_alive():
            self._thread = Thread(target=self.run)
            self._thread.start()
        # self._start_event.set()

    def stop(self):
        if self._thread.is_alive():
            self._stop_event.set()
            self._complet_event.wait()
            self._complet_event.clear()
    
    def setStartValue(self, value):
        with self._lock:
            self.start_value = value

    def setEndValue(self, value):
        with self._lock:
            self.end_value = value

    def setDuration(self, value):
        with self._lock:
            self.duration = value/1000