from PyQt5.QtWidgets import QLabel, QWidget, QSizePolicy, QGraphicsDropShadowEffect,\
                             QOpenGLWidget
from PyQt5.QtGui import QImage, QPixmap, QColor, QGradient, QOpenGLTextureBlitter, \
                        QOpenGLTexture, QPainter
from PyQt5.QtCore import Qt, QEvent, QSize, QRect, QRectF, QPoint, pyqtSlot, \
                        pyqtSignal, QPointF, QSizeF, QEasingCurve

from utils import qevent_filter, stylize, thread, lerp
from GameOS import config
from GameOS.pingpong import PingPongAnim

from threading import RLock, Event
import subprocess
import psutil
import shlex
import time
import os

import OpenGL.GL as gl

DETACHED_PROCESS = 0x00000008

# class GameHeaderBox(QWidget):
#     def __init__(self, parent, name, path, scale):
#         super().__init__(parent)
#         self.name = name
#         self.label = GameHeaderLabel(self, name, path, scale)
#         self.setMaximumSize(int(config.header.max_size_x*scale),
#                             int(config.header.max_size_y*scale))
#         self.clip = False
#         self.show()

class Shadow(QOpenGLWidget):
    texture=None
    def __init__(self, parent):
        super().__init__(parent)
        self.count=0
        # self.image = QImage(config.images.shadow)
    
    def initializeGL(self):
        if not self.__class__.texture:
            image=QImage(config.images.shadow)
            self.__class__.texture = QOpenGLTexture(image)
        self.texture=self.__class__.texture
        
        self.texture.setMinificationFilter(QOpenGLTexture.LinearMipMapLinear)
        self.blitter = QOpenGLTextureBlitter()
        self.blitter.create()
        
    
    # def paintGL(self):

    #     self.count+=1
    #     self.blitter.bind()
    #     target = QOpenGLTextureBlitter.targetTransform(QRectF(0,0,300, 180),
    #                 QRect(0,0,300,180))
    #     gl.glEnable(gl.GL_BLEND) 
    #     gl.glBlendFunc(gl.GL_SRC_ALPHA_SATURATE, gl.GL_ONE)
    #     start=time.perf_counter()
    #     self.blitter.blit(self.texture.textureId(), target,
    #                 QOpenGLTextureBlitter.OriginTopLeft)

    #     gl.glDisable(gl.GL_BLEND)
    #     self.blitter.release()

    # def paintEvent(self, event):
    #     painter = QPainter(self)
    #     rect=QRectF(QPointF(0,0),QSizeF(self.size()))
        # painter.drawImage(rect, self.image,
        #                     QRectF(0,0,300,180))

class GameHeader(QLabel):

    enter_signal = pyqtSignal()
    leave_signal = pyqtSignal()
    run_signal   = pyqtSignal()

    def __init__(self, parent, game, scale):
        super().__init__(parent.parent())
        self.shortcut = game
        self.running_pid = False
        self.default_width=int(config.header.max_size_x*scale)
        self.default_height=int(config.header.max_size_y*scale)
        
        img_path = parent.steam_manager.find_grid_image(game.appid)
        self.pixmap=QPixmap(img_path) if img_path else parent.default_img
        self.setScaledContents(True)
        self.setPixmap(self.pixmap)
        self.zoom=1.2
        self.resize(self.default_width, self.default_height)
        self.updateGeometry()
        self.setMouseTracking(True)

        self._anim_time = 0.2
        self.default_geometry=self.geometry()
        self.zoom_geometry=self._resize(self.zoom)
        self.anim = PingPongAnim(self,b"geometry",self.default_geometry,
                self.zoom_geometry, self._anim_time, QEasingCurve.OutQuad)
        
        self.tileview = parent.parent().parent().parent()
        self.gridlayout = parent

        self.enter_signal.connect(self.enterEvent)
        self.leave_signal.connect(self.leaveEvent)
        self.run_signal.  connect(self.mousePressEvent)
        
    def _resize(self, scale):
        rest=self.default_geometry
        shift=(scale - 1)/2
        h_shift=int(rest.width()*shift)
        v_shift=int(rest.height()*shift)
        return QRect(   QPoint(rest.x()-h_shift, rest.y()-v_shift),
                        QPoint(rest.right()+h_shift, rest.bottom()+v_shift))
            
    def setGeometry(self, rect):
        super().setGeometry(rect)
        self.default_geometry=rect
        self.zoom_geometry=self._resize(self.zoom)
        self.anim.setStartValue(rect)
        self.anim.setEndValue(self.zoom_geometry)

    def sizeHint(self):
        return QSize(self.default_width,self.default_height)
    
    def select(self):
        prev=self.gridlayout.selected.widget()
        prev.leave_signal.emit()
        self.raise_()
        shadow=QGraphicsDropShadowEffect()
        shadow.setBlurRadius(110)
        shadow.setOffset(0,0)
        shadow.setColor(QColor(20,150,255))
        # self.setGraphicsEffect(shadow)
        self.anim.ping()

    def enterEvent(self, event=None):
        self.select()
        self.tileview.focus_sound.play()
        self.gridlayout.select(self)

    def leaveEvent(self, event=None):
        self.anim.pong()
        self.setGraphicsEffect(None)

    def mouseMoveEvent(self, event=None):
        if not self.gridlayout.selected.widget() is self:
            self.enterEvent()

    def mousePressEvent(self, event=None):
        if self.running_pid:
            try:
                process = psutil.Process(self.running_pid)
            except psutil.NoSuchProcess:
                self.running_pid = None
            else:
                if self.shortcut.name in process.exe(): return
                else: self.running_pid = None
        exe = self.shortcut.exe
        options=shlex.split(self.shortcut.options)
        pid = subprocess.Popen([exe,*options],
                cwd=self.shortcut.startdir,
                creationflags=DETACHED_PROCESS).pid
        self.running_pid = pid
        # self.shadow=QGraphicsDropShadowEffect()
        # self.shadow.setBlurRadius(50)
        # self.shadow.setOffset(0,10)
        # self.shadow.setColor(QColor(0,0,0,150))
        # self.
        # self.setGraphicsEffect(self.shadow)
        # self.shadow.setPixmap(shadow_img)
        # self.shadow.setScaledContents(True)
        # self.shadow = Shadow(self.parent())
        # self.shadow.setGeometry(self._resize(1.2))
        # self.shadow.stackUnder(self)
        # self.shadow_img = QImage(config.images.shadow)
        # self.shadow.show()

    # def initializeGL(self):
    #     self.image = QImage(path)
    #     self.texture = QOpenGLTexture(self.image)
    #     # self.texture
    #     # self.texture.setMipLevels(1)
    #     # self.texture.generateMipMaps()
    #     self.texture.setMinificationFilter(QOpenGLTexture.LinearMipMapLinear)
    #     self.blitter = self.parent().blitter

        # self.parent().paint_buffer.emit()

        # self.parent().update()
    #     print(self, time.perf_counter())
    #     painter = QPainter(self.parent())
    #     painter.drawImage(QRectF(self._resize(1.3)),self.shadow_img,
    #                             QRectF(0,0,300,180))
        # super().paintEvent(event)
    
    # def paintGL(self):
    #     self.blitter.bind()
    #     target = QOpenGLTextureBlitter.targetTransform(QRectF(0,0,460, 215),
    #                 QRect(0,0,460, 215))
                    
    #     self.blitter.blit(self.texture.textureId(), target,
    #                 QOpenGLTextureBlitter.OriginTopLeft)
    #     self.blitter.release()
        # self.shadow.setBlurRadius(50)
        # self.shadow.setOffset(0,0)
        # self.shadow.setColor(QColor(255,0,0))
        # self.shadow.setColor(QColor(50,170,255))
        
        # self.shadow.setBlurRadius(50)
        # self.shadow.setOffset(0,10)
        # self.shadow.setColor(QColor(0,0,0,150))
    
    # def _animate(self):
    #     rest=self.default_geometry
    #     target=self.zoom_geometry
    #     x = lerp(target.x(), rest.x(), self._zoom)
    #     y = lerp(target.y(), rest.y(), self._zoom)
    #     width   = lerp(target.width(),  rest.width(),  self._zoom)
    #     height  = lerp(target.height(), rest.height(), self._zoom)
    #     super().setGeometry(x,y,width,height)
        

    # def _check_anim_flag(self):
    #     self._stop_event.set()
    #     self._start_event.wait()
    #     self._start_event.clear()
    #     self._stop_event.clear()

    # @thread
    # def zoom_in(self):
    #     self._check_anim_flag()
    #     start=time.clock()
    #     left=self._anim_time*(1-self._zoom)
    #     while self._zoom < 1:
    #         if self._stop_event.is_set(): #wait(1/self._timestep):
    #             self._stop_event.clear()
    #             break
    #         self._zoom = min((time.clock() - start) / left, 1)
    #         self._animate()
    #     self._start_event.set()

    # @thread
    # def zoom_out(self):
    #     self._check_anim_flag()
    #     start=time.clock()
    #     left=self._anim_time*self._zoom
    #     start_val=self._zoom
    #     while self._zoom:
    #         if self._stop_event.is_set(): #wait(1/self._timestep):
    #             self._stop_event.clear()
    #             break
    #         self._zoom = max(start_val - (time.clock() - start) / left, 0)
    #         self._animate()
    #     self._start_event.set()

    #@qevent_filter(QEvent.Enter)
    # def event(self, event):
    #     print(self.count)
    #     self.count += 1