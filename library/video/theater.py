from PyQt5.QtWidgets import QWidget, QOpenGLWidget, QGraphicsOpacityEffect
from PyQt5.QtGui import QImage, QOpenGLTextureBlitter, QOpenGLTexture, \
                        QOpenGLContext, QColor, QPainter, QRadialGradient, QBrush, QLinearGradient
from PyQt5.QtCore import Qt, QEvent, QSize, QRect, QRectF, QPointF, QPoint, pyqtSlot, pyqtSignal
from PyQt5.QtMultimedia import QSound
from ..library import LibraryView

class Theater(LibraryView):
    def __init__(self, name):
        super().__init__(name)
        self.setStyleSheet("background-color:blue")