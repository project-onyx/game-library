import sys
import os

from . import config

from PyQt5.QtWidgets import QMainWindow, QGridLayout, QWidget, QFrame
from PyQt5.QtCore import Qt, QRect, QUrl, QEvent
from PyQt5.QtMultimedia import QMediaContent, QMediaPlaylist, QMediaPlayer

from utils import lock
from .library import Library

from . import common

# gridpath=r"C:\Users\Matthieu\Pictures\caca"
# print(sys.modules.keys())

# @stylize(config)
class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        # self.setWindowTitle("caca")

        # self.setWindowFlags(Qt.FramelessWindowHint |
        #                     Qt.WindowStaysOnTopHint )
        
        # self.setAutoFillBackground(True)
        # self.move(0,0)
        
        # self.tiles_view.show()
        # self.grid_widget.setGeometry(self.frameGeometry())
        # self.scroll_area.setGeometry(self.frameGeometry())
        # self.grid_layout.setGeometry(self.frameGeometry())
        # self.grid_widget.hide()
        # self.setCentralWidget(self.scroll_area)
        self.play_theme()

        self.setGeometry(0,0,1920*0.9,1080*0.9)
        self.library = Library(self)
        self.setCentralWidget(self.library)
        self.installEventFilter(self)
        self.showFullScreen()
        # self.show()
    
    def play_theme(self):
        url = QUrl(":/sound/theme")
        self.playlist = QMediaPlaylist()
        self.playlist.addMedia(QMediaContent(url))
        self.playlist.setPlaybackMode(QMediaPlaylist.Loop)
        self.player = QMediaPlayer()
        self.player.setPlaylist(self.playlist)
        self.player.play()

    def resize(self, *args, **kwargs):
        super().resize( *args, **kwargs)
        self.tiles_view.setGeometry(self.frameGeometry())
    
    def eventFilter(self, object, event):
        if event.type() == QEvent.WindowActivate:
            self.library.enable()
            self.player.play()
        elif event.type() == QEvent.WindowDeactivate:
            self.library.disable()
            self.player.pause()
        return False