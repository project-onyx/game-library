import time
import os
import sys

from utils import is_linux, register
from rpyc.utils.classic import unix_connect
from rpyc import connect

from .config import Client

class ClientError(Exception):
    pass

class GameManagerClient:

    def __init__(self):
        self.config = Config(join_module_path("config.cfg"))
        try:
            if is_linux():
                self.rpc = unix_connect(Client.unix_socket)
            else:
                self.rpc = connect(Client.host,Client.port)
        except OSError as e:
            raise ClientError(
                f"connot connect to GameManager daemon : {e}")
        else:
            self.root = self.rpc.root
            self._register_functions()

    def _register_functions(self):
        for key, value in vars(self).items():
            id = getattr(value, "_id")
            if id and id == key:
                setattr(self, key, getattr(self.root, key))

    @register
    def sync_games(self):
        "synchronize installed games with database"
    
    @register
    def add_game(self, game):
        "add new game to database"

    @register
    def remove_game(self, game):
        "remove game from database"

    @register
    def update_games(self):
        "update game database"

    @register
    def start_game(self, game):
        "start new game instance"

    @register
    def stop_game(self, instance):
        "stop game instance"

    @register
    def list_games(self):
        "list all games"

            # print("finished checking")
            # self._watchers.append(on_created(*self.watchlist)(self.event_wrapper(self.create)))
            # self._watchers.append(on_deleted(*self.watchlist)(self.event_wrapper(self.remove)))
            # self._watchers.append(on_moved(*self.watchlist)(self.move_wrapper(self.move)))
 
    # def wrap_event(self, handle, fsevent):
    #     def _cd_wrapper(event):
    #         fsevent(event)
    #         if handle(self.get_dest_path(event.src_path)):
    #             self.save_games()
    #     def _m_wrapper(event):
    #         fsevent(event)
    #         if handle(  self.get_dest_path(event.src_path),
    #                     self.get_dest_path(event.dest_path)):
    #             self.save_games()
    #     if fsevent.__name__ == "move_link":
    #         return _m_wrapper
    #     return _cd_wrapper

    # def get_game_name(self, path):
    #     rel = os.path.relpath(path, self.gameroot)
    #     return get_first(rel)
    
    # def create_game(self, path):
    #     sc = self.create_game(path)
    #     if sc : return self.get_grid_image(sc)

    # def delete_game(self, path):
    #     appid = self.delete_game(path)
    #     return self.delete_grid_image(appid)
    
    # def move_game(self, oldpath, newpath):
    #     sc = self.move_game(oldpath, newpath)
    #     if sc: return self.move_grid_image(sc, oldpath, newpath)

