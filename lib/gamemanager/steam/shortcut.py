import sys
import os

from game import Game
from crc_algorithms import Crc

class Shortcut:

    def __init__(self, name, exe, startdir, icon="", tags=list(), options=""):

        self.name = name
        self.exe = exe
        self.startdir = startdir
        self.icon = icon
        self.tags = tags
        self.appid = self.get_appid()
        self.options = options
        
    def get_appid(self, exe=None, name=None):
        exe = exe or self.exe
        name = name or self.name
        data = f'"{exe}"{name}'
        algorithm = Crc(width = 32, poly = 0x04C11DB7, reflect_in = True,
                        xor_in = 0xffffffff, reflect_out = True, xor_out = 0xffffffff)
        top_32 = algorithm.bit_by_bit(data) | 0x80000000
        full_64 = (top_32 << 32) | 0x02000000
        return str(full_64)

    def __repr__(self):
      return f"game(name:{self.name}\n exe:{self.exe}\n startdir:{self.startdir}\n\
             icon:{self.icon}\n tags:{self.tags}\n appid:{self.appid})"

    def __eq__(self, other):
      if type(other) == Game:
        return self.exe == other.exe
      else:
        return self.exe == other