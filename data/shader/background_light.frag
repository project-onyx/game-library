#version 430
#extension GL_ARB_explicit_uniform_location : enable

out vec4 color;
in vec2 pos;
layout(location=2) uniform vec3 in_color;
layout(location=3) uniform vec3 out_color;


float quadInOut(float t){
    if (t < 0.5)
        return 2 * pow(t, 2.0);
    return -2 * pow(t, 2.0) + 4 * t - 1;
}
void main()
{
    float dist = min(1,length(pos));
    float factor = quadInOut(1-dist);
    color = vec4(mix(in_color, out_color, factor),1);
}