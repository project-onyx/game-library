#version 430
#extension GL_ARB_explicit_uniform_location : enable

out vec2 pos;
layout(location=0) in vec2 vertex_pos;
layout(location=1) uniform mat3 transform;

void main()
{
    gl_Position.xyz = vec3(vertex_pos,1)*transform;
    pos = vertex_pos;
}