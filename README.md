# GameOS

This project aims to provide a game console experience for PC users who wants
the best of both worlds in a couch and TV comfort, ideal for multimedia platforms.
It code if almost exclusively written in python for cross-platform and maintainablity
reasons. The GUI relies mainly on Qt framework witch included a home launcher for
starting apps, a library for managing installed games, video...

You can install it on Windows or Linux, for witch is complete desktop environment
including a custom window manager, boot animation.
Note that the interface has full gamepad support.

The library will automatically seek for installed game from steam or any other source.
Any found game will automatically be added to the library.
This module use https://gitlab.com/MatthieuJacquemet/gamemanager to automatically
identify steam games or non steam games and use a scrapper to add information such
a game type, release date, image header...
