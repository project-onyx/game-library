from watchdog import events
from watchdog.observers import Observer
import os
from utils_types import FileSystemEvent as event_types
from utils import get_at, set_at

class DummyEvent:
    def __init__(self, src, dest=None):
        self.src_path = src
        self.dest_path = dest

class FolderUnion(events.FileSystemEventHandler):

    def __init__(self, union_path, *sources, blacklist=list(),
                recursive=False):

        self._pools         = list()
        self._watchers      = list()
        self._sources       = sources
        self.blacklist      = blacklist
        self.recursive      = recursive
        self.union_path     = union_path

        self.register_handler(event_types.CREATED)(self.create_link)
        self.register_handler(event_types.DELETED)(self.delete_link)
        self.register_handler(event_types.MOVED)(self.move_link)

    def register_handler(self, type):
        def factory(func):
            pool = get_at(self._pools, type)
            if pool == None:
                pool = list()
            set_at(self._pools, pool.append(func), type, None)
            return func
        return factory
        
    def start(self):
        for path in self._sources:
            observer = Observer()
            observer.schedule(self, path, self.recursive)
            observer.start()
            self._watchers.append(observer)
        if self._sources:
            self.check_union()
    
    def check_union(self):
        for i in os.listdir(self.union_path):
            if not self._is_in_source(i):
                self.delete_link(i)
        for src in self._sources:
            for i in os.listdir(src):
                fullpath = os.path.join(src, i)
                if not (os.path.isfile(fullpath) or i.startswith(".") or i in self.blacklist):
                    self.create_link(fullpath)
    
    def _is_in_source(self, name):
        for i in self._sources:
            if os.path.exists(os.path.join(i, name)):
                return True
        return False

    def _get_dest_path(self, path):
        folder = os.path.basename(path)
        return os.path.join(self.union_path, folder)

    def create_link(self, path):
        link = self._get_dest_path(path)
        if not os.path.islink(link):
            os.symlink(path, link)
    
    def delete_link(self, path):
        link = self._get_dest_path(path)
        if os.path.islink(link):
            os.unlink(link)
    
    def move_link(self, oldpath, newpath):
        self.delete_link(oldpath)
        self.create_link(newpath)

    def on_created(self, event):
        pool = get_at(self._pools, event_types.CREATED)
        if not pool:
            return
        for func in pool:
            func(event.src_path)

    def on_deleted(self, event):
        pool = get_at(self._pools, event_types.DELETED)
        if not pool:
            return
        for func in pool:
            func(event.src_path)

    def on_moved(self, event):
        pool = get_at(self._pools, event_types.MOVED)
        if not pool:
            return
        for func in pool:
            func(event.src_path, event.dest_path)