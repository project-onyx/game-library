from utils import contains, get_filename
import os
from difflib import SequenceMatcher
import re

def _find_patterns(path, patterns):
    for i in patterns:
        if re.search(i, path):
            return True
    return False

def _find_siblings(path, siblings):
    folder = os.path.dirname(path)
    for file in os.listdir(folder):
        if file in siblings:
            return True
    return False

def _find_acronym(filename, reference_name):
    acronym = "".join(i[0] for i in reference_name.split(" ")).lower()
    return SequenceMatcher(None, acronym, filename.lower()).ratio()

def _list_exe(path, folder_blacklist, file_blacklist):
    found = list()
    for entity in os.listdir(path):
        fullpath = os.path.join(path, entity)
        if os.path.isfile(fullpath):
            if os.path.splitext(entity)[-1] == ".exe":
                if not contains(entity.lower(), file_blacklist):
                    found.append((os.path.getsize(fullpath), fullpath))
        elif not contains(entity.lower(), folder_blacklist):
            found.extend(_list_exe(fullpath, folder_blacklist, file_blacklist))
    return found

def find_exe(path, siblings=[], patterns=[], folder_blacklist=[], file_blacklist=[]):
        
        found_exes = _list_exe(path, folder_blacklist, file_blacklist)
        
        if found_exes:
            
            max_score = 0
            found_path = None
            num = len(found_exes)

            for i,(_, exe_path) in enumerate(sorted(found_exes)):
                name            = get_filename(path)
                search_name     = os.path.basename(path)

                size_score      = i/num
                name_score      = SequenceMatcher(None, name, search_name).ratio()
                pattern_score   = _find_patterns(exe_path, patterns)
                siblings_score  = _find_siblings(exe_path, siblings)
                acronym_score   = _find_acronym(name, search_name)
                position_score  = os.path.dirname(exe_path) == path

                score = name_score + size_score*0.9 + pattern_score*0.3 + \
                        siblings_score*0.3 + acronym_score*0.5 + position_score*1.0

                if score > max_score:
                    max_score = score
                    found_path = exe_path
            
            return found_path

        # print(found_path, "size:%s name:%s pat%s dll:%s acro:%s pos:%s"%(size_rank, name_rank, pattern, dlls, acronym, position))
    
