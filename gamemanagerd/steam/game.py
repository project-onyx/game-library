import sys
import os
import shutil

from core import GameManagerGame

class Game(GameManagerGame):

    @staticmethod
    def valid_custom_image_extensions():
        return ['.png', '.jpg', '.jpeg', '.tga']

    def __init__(self, appid):
        self._appid = appid

    def __eq__(self, other):
        return (
            isinstance(other,self.__class__) and
            self.appid() == other.appid())

    def __hash__(self):
        return "__GAME{0}__".format(self.appid()).__hash__()

    def _custom_image_path(self, user, extension):
        filename = self.appid() + extension
        return os.path.join(user.grid_directory(), filename)

    def appid(self):
        return str(self._appid)

    def custom_image(self, user):
        for ext in self.valid_custom_image_extensions():
            image_location = self._custom_image_path(user, ext)
            if os.path.isfile(image_location):
                return image_location
        return None

    def set_image(self, user, image_path):
        _, ext = os.path.splitext(image_path)
        shutil.copy(image_path, self._custom_image_path(user, ext))
