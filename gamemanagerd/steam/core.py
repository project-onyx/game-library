import os
import sys

import user
import shortcut

from exefinder import find_exe
from core import GameManagerCore

from utils import remove_first, common_suffix, \
    is_linux, is_windows, is_mac
    
class SteamCore(GameManagerCore):

    def __init__(   self,
                    siblings=list(),
                    matchpattern=list(),
                    blacklistfolder=list(),
                    blacklist=list(),
                    default_exe_cwd=True, 
                    default_options="",
                    steam_location=None
            ):
        self.user = user.User(self.steam_context, self.userid)
        self.siblings = siblings
        self.match_pattern = matchpattern
        self.blacklist_folder = blacklistfolder
        self.blacklist_names = blacklist
        self.games = self.load_games()
        self.default_exe_cwd = default_exe_cwd
        self.default_options = default_options

        if not steam_location and is_windows():
            steam_location = self.windows_steam_location()
        self.steam_location = steam_location

    def windows_steam_location(self):
        if not is_windows():
            return
        import winreg as registry
        key = registry.OpenKey(registry.HKEY_CURRENT_USER,R"Software\Valve\Steam")
        value, *_ = registry.QueryValueEx(key,"SteamPath")
        registry.CloseKey(key)
        return value

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__) and
            self.userdata_location() == other.userdata_location()
        )

    def userdata_location(self):
        if is_windows():
            return os.path.join(self.steam_location, "userdata")
        elif is_mac():
            return os.path.join(os.path.expanduser("~"),
                "Library","Application Support","Steam","userdata")
        elif is_linux():
            return os.path.join(os.path.expanduser("~"),
                ".local","share","Steam","userdata")
        else:
            raise EnvironmentError("Running on unsupported environment %s" % sys.platform)

    def local_users(self):
        users = list()
        userdata_dir = self.userdata_location()
        for entry in os.listdir(userdata_dir):
            if os.path.isdir(os.path.join(userdata_dir,entry)):
                u = user.User(self, int(entry))
                users.append(u)
        return users
    
    def find_user(self):
        userdata_dir = self.userdata_location()
        for entry in os.listdir(userdata_dir):
            if os.path.isdir(os.path.join(userdata_dir,entry)):
                if user._community_id_is_64(self.userid):
                    id32 = user._community_id_32_from_64(self.userid)
                    id64 = self.userid
                else:
                    id32 = self.userid
                    id64 = user._community_id_64_from_32(self.userid)
                if entry in (str(id32), str(id64)):
                    return True

    def create_game(self, path, tags=[]):
        if self.find_games(path) == None:
            found = find_exe(path, self.siblings,self.match_pattern,
                                self.blacklist_folder,self.blacklist_names)
            if not found == None:
                print(found)
                name = os.path.basename(path)
                startdir = path
                if self.default_exe_cwd:
                    startdir = os.path.dirname(found)
                sc = shortcut.Shortcut(name, found, startdir, tags=tags,
                                options=self.default_options)
                self.games.append(sc)
                return sc
            print("cannot find game executable for :", path)

    def delete_game(self, path):
        found = self.find_games(path)
        if not found == None:
            appid = found.appid
            self.games.remove(found)
            return appid
    
    def relocate_game(self, oldpath, newpath):
        found = self.find_games(oldpath)
        if not found == None:
            relative = os.path.relpath(found.exe, oldpath)
            found.exe = os.path.join(newpath, relative)
            found.startdir = os.path.dirname(found.exe)
            found.name = os.path.basename(newpath)
            found.appid = found.get_appid()
            return found
        return self.create_game(newpath)
    
    def save_games(self):
        self.user.shortcuts = self.games
        self.user.save_shortcuts()

    def load_games(self):
        return self.user._load_shortcuts()