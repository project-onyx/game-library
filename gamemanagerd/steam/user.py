import sys
import os

from ._shortcut_parser import ShortcutParser
from ._shortcut_generator import ShortcutGenerator

individual_account_type_identifier = 0x0110000100000000

def _community_id_is_64(communityid):
    return communityid > individual_account_type_identifier

def _community_id_32_from_64(communityid64):
    return communityid64 - individual_account_type_identifier

def _community_id_64_from_32(communityid32):
    return communityid32 + individual_account_type_identifier

class User:

    def __init__(self, steam, userid):
        self.steam = steam

        if _community_id_is_64(userid):
            self.id32 = _community_id_32_from_64(userid)
            self.id64 = userid
        else:
            self.id32 = userid
            self.id64 = _community_id_64_from_32(userid)

        self.shortcuts = self._load_shortcuts()
    
    def __eq__(self, other):
        return (
            isinstance(other,self.__class__) and
            self.id32 == other.id32
        )

    def _user_config_directory(self):
        return os.path.join(
            self.userdata_directory(),
            "config"
        )

    def _load_shortcuts(self):
        parsed_shortcuts = list()
        try:
            parser = ShortcutParser(self.shortcuts_file())
            parsed_shortcuts = parser.shortcuts
        except IOError:
            pass
        return parsed_shortcuts

    def userdata_directory(self,):
        return os.path.join(
            self.steam.userdata_location(),
            str(self.id32)
        )

    def shortcuts_file(self):
        return os.path.join(self._user_config_directory(), "shortcuts.vdf")
    
    def grid_directory(self):
        return os.path.join(self._user_config_directory(), "grid")

    def save_shortcuts(self, path=None, makedirs=True):
        path = path or self.shortcuts_file()
        parent_directory = os.path.dirname(path)
        if not os.path.isdir(parent_directory) and makedirs:
            try:
                os.makedirs(parent_directory)
            except OSError:
                raise OSError(f"Cannot write to directory {parent_directory}.")
        try:
            contents = ShortcutGenerator().to_string(self.shortcuts)
            with open(path, "wb") as file:
                file.write(contents)
        except IOError:
            raise IOError("Cannot save file to {path}. Permission Denied")