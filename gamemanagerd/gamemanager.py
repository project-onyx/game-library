import time
import os
import sys
import glob

from pysteam import user
from pysteam import shortcut
from pysteam import steam

from utils import wait_forever, get_first, token
from utils_types import FileSystemEvent as event_types

from .folderunion import FolderUnion
from .exefinder import find_exe
from .imageloader import ImageProvider
from ._utils import run_service

from rpyc import Service

class ConfigError(Exception):
    pass

@run_service
class GameManager(Service):

    def __init__(self,  gameroot="/usr/local/gamemanager/games",
                        folderblacklist=list(),
                        siblings=list(),
                        match_patterns=list(),
                        folder_blacklist=list(),
                        file_blacklist=list(),
                        default_exe_cwd=True,
                        default_options="",
                        image_providers=list()):

            self.gameroot = gameroot
            
            if self.find_user():
                self.user = user.User(self.steam_context, self.userid)
                self.games = self.load_games()
                self.grid_path = self.user.grid_directory()

                self.check_union()
                print("union checked")
                self.sync_games()
                print("game synced")
                self.sync_images()
                print("images synced")
                
                self.start()
                # wait_forever()
            else:
                print("cannot find user with id :", self.userid)
    
    
    def get_config(self, xpath):
        values = list()
        items = self.configtree.findall(xpath)
        if not len(items):
            raise ConfigError(f"no config found for {xpath}")
        if len(items) == 1:
            return token(items[0].text)
        for item in items:
            values.append(token(item.text))
        return values
    
    def sync_games(self):
        for game in self.games:
            if not os.path.exists(game.exe):
                self.games.remove(game)
        for folder in os.listdir(self.gameroot):
            self.create_game(os.path.join(self.gameroot, folder))
        self.save_games()

    def on_created(self, event):
        self.union.create_link(event.src_path)
        sc = self.create_game(event.src_path)
        if sc : return self.get_grid_image(sc.name, sc.appid)
    
    def on_modified(self, event):
        self.union.delete_link(event.src_path)
        appid = self.delete_game(event.src_path)
        return self.delete_grid_image(appid)

    def on_moved(self, event):
        self.union.move_link(event.src_path, event.dest_path)
        src = self.get_dest_path(event.src_path)
        des = self.get_dest_path(event.dest_path)
        shortcut = self.relocate_game(src, des)
        if shortcut: return self.move_grid_image(shortcut, src, des)
    
    def get_dest_path(self, path, basepath=None):
        if basepath == None:
            relative = os.path.basename(path)
        else:
            relative = os.path.relpath(path, os.path.dirname(basepath))
        return os.path.join(self.gameroot, relative)


            # print("finished checking")
            # self._watchers.append(on_created(*self.watchlist)(self.event_wrapper(self.create)))
            # self._watchers.append(on_deleted(*self.watchlist)(self.event_wrapper(self.remove)))
            # self._watchers.append(on_moved(*self.watchlist)(self.move_wrapper(self.move)))
 
    # def wrap_event(self, handle, fsevent):
    #     def _cd_wrapper(event):
    #         fsevent(event)
    #         if handle(self.get_dest_path(event.src_path)):
    #             self.save_games()
    #     def _m_wrapper(event):
    #         fsevent(event)
    #         if handle(  self.get_dest_path(event.src_path),
    #                     self.get_dest_path(event.dest_path)):
    #             self.save_games()
    #     if fsevent.__name__ == "move_link":
    #         return _m_wrapper
    #     return _cd_wrapper

    # def get_game_name(self, path):
    #     rel = os.path.relpath(path, self.gameroot)
    #     return get_first(rel)
    
    # def create_game(self, path):
    #     sc = self.create_game(path)
    #     if sc : return self.get_grid_image(sc)

    # def delete_game(self, path):
    #     appid = self.delete_game(path)
    #     return self.delete_grid_image(appid)
    
    # def move_game(self, oldpath, newpath):
    #     sc = self.move_game(oldpath, newpath)
    #     if sc: return self.move_grid_image(sc, oldpath, newpath)

