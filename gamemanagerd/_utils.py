from rpyc.utils.server import Server
from gamemanager.config import Client
from utils import is_linux

def run_service(cls):
    if is_linux():
        srv = Server(cls, socket_path=Client.unix_socket)
    else:
        srv = Server(cls, port=Client.port)
    srv.start()