#!/usr/bin/env python
# encoding: utf-8
"""
_shortcut_parser.py

Created by Scott on 2013-12-29.
Copyright (c) 2013 Scott Rice. All rights reserved.
"""

import sys
import os
import re
import vdf

from . import shortcut

class ShortcutParser(object):
    
    def _list_shortcuts(self, items):
        for app in items.values():
            yield {k.lower():v for k,v in app.items()}
        
    def __init__(self, path):
        self.shortcuts = []
        with open(path, "rb") as file:
            self.data = vdf.binary_loads(file.read())
            # try:
            shortcut_list = self.data["shortcuts"]
            for app in self._list_shortcuts(shortcut_list):
                exe         = app.get("exe","")[1:-1]
                icon        = app.get("icon","")
                appname     = app.get("appname","")
                startdir    = app.get("startdir","")[1:-1]
                options     = app.get("launchoptions","")
                tags        = [v for v in app["tags"].values()]
                self.shortcuts.append(shortcut.Shortcut(appname,exe,startdir,icon,tags,options))
                    # playtime    = app["LastPlayTime"]
                    # options     = app["LaunchOptions"]
                    # hidden      = app["IsHidden"]
                    # deskconf    = app["AllowDesktopConfig"]
                    # overlay     = app["AllowOverlay"]
                    # openvr      = app["OpenVR"]
                    # devkit      = app["Devkit"]
                    # dkgameid    = app["DevkitGameID"]
            # except KeyError as e:
            #     print(e)
            #     self.shortcuts = None

    # def parse(self, path):
    #     if not os.path.exists(path):
    #         raise IOError("Shortcuts file '%s' does not exist" % path)
    #     file_contents = open(path, "rb").read()
    #     return self.match_base(file_contents)

    # def match_base(self,string):
    #         match = re.match(b"\000shortcuts\000(.*)\008\008$",string, re.IGNORECASE)
    #         if match:
    #             return self.match_array_string(match.groups()[0])
    #         else:
    #             return None

    # def match_array_string(self,string):
    #     # Match backwards (aka match last item first)
    #     if string == "":
    #         return []
    #     # One side effect of matching this way is we are throwing away the
    #     # array index. I dont think that it is that important though, so I am
    #     # ignoring it for now
    #     shortcuts = []
    #     while True:
    #         match = re.match(b"(.*)\000[0-9]+\000(\001AppName.*)\008",string, re.IGNORECASE)
    #         if match:
    #             groups = match.groups()
    #             string = groups[0]
    #             shortcuts.append(self.match_shortcut_string(groups[1]))
    #         else:
    #             shortcuts.reverse()
    #             return shortcuts

    # def match_shortcut_string(self,string):
    #     # I am going to cheat a little here. I am going to match specifically
    #     # for the shortcut string (Appname, Exe, StartDir, etc), as oppposed
    #     # to matching for general Key-Value pairs. This could possibly create a
    #     # lot of work for me later, but for now it will get the job done
    #     match = re.match(b"\001AppName\000(.*)\000\001Exe\000(.*)\000\001StartDir\000(.*)\000\001icon\000(.*)\000\000tags\000(.*)\008",string, re.IGNORECASE)
    #     if match:
    #         # The 'groups' that are returned by the match should be the data
    #         # contained in the file. Now just make a Shortcut out of that data
    #         groups = match.groups()
    #         appname = groups[0]
    #         exe = groups[1]
    #         startdir = groups[2]
    #         icon = groups[3]
    #         tags = self.match_tags_string(groups[4])
    #         return shortcut.Shortcut(appname,exe,startdir,icon,tags)
    #     else:
    #         return None

    # def match_tags_string(self,string):
    #     tags = []
    #     while True:
    #       match = re.match(b"(.*)\u0001[0-9]+\u0000(.*?)\u0000",string)
    #       if match:
    #           groups = match.groups()
    #           string = groups[0]
    #           tag = groups[1]
    #           tags.append(tag)
    #       else:
    #           tags.reverse()
    #           return tags