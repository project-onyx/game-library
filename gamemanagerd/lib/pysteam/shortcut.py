#!/usr/bin/env python
# encoding: utf-8
"""
game.py

Created by Scott on 2013-12-28.
Copyright (c) 2013 Scott Rice. All rights reserved.
"""

import sys
import os

from . import game
from ._crc_algorithms import Crc

class Shortcut(game.Game):

    def __init__(self, name, exe, startdir, icon="", tags=[], options=""):

        self.name = name
        self.exe = exe
        self.startdir = startdir
        self.icon = icon
        self.tags = tags
        self.appid = self.get_appid()
        self.options = options
        
    def get_appid(self, exe=None, name=None):
        algorithm = Crc(width = 32, poly = 0x04C11DB7, reflect_in = True,
                        xor_in = 0xffffffff, reflect_out = True, xor_out = 0xffffffff)
        if not exe: exe = self.exe
        if not name: name = self.name
        crc_input = f'"{exe}"{name}'
        top_32 = algorithm.bit_by_bit(crc_input) | 0x80000000
        full_64 = (top_32 << 32) | 0x02000000
        return str(full_64)

    def __repr__(self):
      return f"game(name:{self.name}\n exe:{self.exe}\n startdir:{self.startdir}\n\
             icon:{self.icon}\n tags:{self.tags}\n appid:{self.appid})"

    def __eq__(self, other):
      if type(other) == game:
        return self.exe == other.exe
      else:
        return self.exe == other