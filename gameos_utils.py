from PyQt5.QtGui import QColor, QLinearGradient, QRadialGradient, QTransform, \
    QMatrix2x2
# from PyQt5.QtCore import
from utils import lerp, token
import xml.etree.ElementTree as ET
import math
import sys
import OpenGL.GL as gl
from OpenGL.arrays import vbo
import ctypes

class EasingGradiant(QLinearGradient):
    def __init__(self, start, end, color_in, color_out, step=5):
        super().__init__(start, end)
        for i in range(step):
            pos=1/(step-1) * i
            factor = (1-pos)**2#self.quad_in_out(pos)
            r = lerp(color_in.red(),    color_out.red(),    factor)
            g = lerp(color_in.green(),  color_out.green(),  factor)
            b = lerp(color_in.blue(),   color_out.blue(),   factor)
            a = lerp(color_in.alpha(),  color_out.alpha(),  factor)
            self.setColorAt(pos, QColor(r,g,b,a))

    def quad_in_out(self, t):
        if t < 0.5:
            return 2 * t * t
        return (-2 * t * t) + (4 * t) - 1


class EasingRadialGradiant(QRadialGradient):
    def __init__(self, color_in, color_out, step=5):
        super().__init__()
        for i in range(step):
            pos=1/(step-1) * i
            factor = self.quad_in_out(1-pos)
            r = lerp(color_in.red(),    color_out.red(),    factor)
            g = lerp(color_in.green(),  color_out.green(),  factor)
            b = lerp(color_in.blue(),   color_out.blue(),   factor)
            a = lerp(color_in.alpha(),  color_out.alpha(),  factor)
            self.setColorAt(pos, QColor(r,g,b,a))

    def quad_in_out(self, t):
        if t < 0.5:
            return 2 * t * t
        return (-2 * t * t) + (4 * t) - 1


class PrimitiveRect:

    def __init__(self, size_x=1, size_y=1):
        pass


class PrimitiveDisc:

    def __init__(self, radius, segment=8):
        angle = math.pi*2/segment
        r = 1/math.cos(angle/2)
        verts = [0.0]*2
        index = list()
        for i in range(segment):
            x = math.cos(i*angle)*r
            y = math.sin(i*angle)*r
            verts.extend([x,y])
            if i: index.extend([0,i,i+1])
        index.extend([0,segment, 1])

        self.radius = radius
        self.segment = segment
        self.vertex_buffer = create_vbo(verts, gl.GL_ARRAY_BUFFER, ctypes.c_float)
        self.index_buffer  = create_vbo(index, gl.GL_ELEMENT_ARRAY_BUFFER, ctypes.c_int)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.vertex_buffer)
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.index_buffer)

    def draw(self):
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.vertex_buffer)
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.index_buffer)
        gl.glEnableVertexAttribArray(0)
        gl.glVertexAttribPointer(0, 2, gl.GL_FLOAT, False, 0, None)
        gl.glDrawElements(gl.GL_TRIANGLES, self.segment*3, gl.GL_UNSIGNED_INT, None)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, 0)
        
def create_vbo(data, type, dtype):
    buffer = gl.glGenBuffers(1)
    gl.glBindBuffer(type, buffer)
    gl.glBufferData(type, len(data)*4, 
            (dtype*len(data))(*data), gl.GL_STATIC_DRAW)
    return buffer

def transform_2d(pos_x, pos_y, size_x, size_y, view_x, view_y):
    ratio=view_x/view_y
    transform = QTransform()
    transform.translate(pos_x*2/view_x-1, (pos_y*-2/view_y+1))
    transform.scale(size_x/view_x*2,(size_y/view_x)*ratio*2)
    return transform.transposed()

def list_properties(tree):
    properties = dict()
    if tree:
        for child in tree:
            if child.tag == "property":
                id = child.attrib.get("id")
                if not id:
                    continue
                properties[id] = token(child.text)
    return properties

def load_data(path):
    tree = ET.parse(path)
    root = tree.getroot()
    datas = dict()

    for section in root:
        datas[section.tag] = section
    return datas