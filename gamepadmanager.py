#!python
import time
import os
from threading import RLock

from utils import main, Profiler, register, State, \
    thread, set_at, get_at, on_created
from utils_types import StateType

from evdev import InputDevice, ecodes, list_devices

profiler = Profiler()

class GamepadError(Exception):
    pass

class ButtonError(GamepadError):
    pass

class AxisError(GamepadError):
    pass

class StateError(GamepadError):
    pass

class CodeError(GamepadError):
    pass


class GamepadManager:

    def __init__(self, id, ignored_events=[ecodes.EV_SYN,ecodes.EV_MSC]):
        
        self.handlers = self.__class__.handlers
        self.ignored_events = ignored_events
        self.access_lock = RLock()
        self.callbacks  = list()
        self.any_event  = list()
        self.id = id

        self.plug_event(id)
        on_created("/dev/input")(self.plug_event)

    def __new__(cls, *args, **kwargs):
        cls.handlers = list()
        for method in vars(cls).values():
            id = getattr(method, "_id", None)
            if id:
                set_at(cls.handlers, method, id, None)
        return super().__new__(cls,*args, **kwargs)

    def plug_event(self, path):
        for dev_path in list_devices():
            dev = InputDevice(dev_path)
            if dev.name == id:
                self.device = dev
                self.event_loop()

    @thread
    def event_loop(self):
        any_event=None
        for event in self.device.read_loop():
            if not event.type in self.ignored_events:
                self.dispatch(event)
        
                for callback in self._any:
                    callback(any_event.ev_type, any_event.code,
                                any_event.state)

    def dispatch(self, event):
        handle = get_at(self.handlers, event.ev_type)
        if handle:
            return handle(self, event.code, event.value, event.timestamp)
        return False

    @register(ecodes.EV_ABS)
    def analog(self, code, state, timestamp):
        value = state/255 - 0.5
        cb = get_at(self.callbacks, code)
        if cb == None:
            return
        for callback in cb[0]:
            callback(value, code)

    @register(ecodes.EV_KEY)
    def key(self, code, state, timestamp):
        
        cb = get_at(self.callbacks, code)
        if cb == None:
            return
        for callback in cb[state]:
            callback(state, code)

    def register_callback(self, callback, code, state):
        cb = get_at(self.callbacks, code)
        if cb == None:
            set_at(self.callbacks, ([], [], []), code, None)
        try:
            self.callbacks[code][state].append(callback)
        except IndexError:
            raise StateError("invalid state value")

    def unregister_callback(self, callback, code, state):
        try:
            self.callbacks[code][state].remove(callback)
        except (IndexError, KeyError, ValueError):
            print(f"cannot unregister {callback} {code} {state}")

    def register_any(self, callback):
        self.any_event.append(callback)

    def unregister_any(self, callback):
        self.any_event.remove(callback)