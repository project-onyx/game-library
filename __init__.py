import os
import sys

from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtCore import Qt, QEvent, pyqtSignal, QFile
from PyQt5.QtGui import QCursor

from utils import main, parse_args, Config
from .mainwindow import MainWindow
from .gameos_utils import list_properties, load_data
from .gamepadmanager import GamepadManager

from . import config
from . import common

from utils import lock, join_module_path

@main(**parse_args(sys.argv))
class GameManager(QApplication):

    gamepad_signal = pyqtSignal()
    mouse_signal   = pyqtSignal()

    def __init__(self, exe=None):
        super().__init__([])
        datas = load_data(join_module_path("data.xml"))
        properties = list_properties(datas.get("properties"))

        common.app=self
        common.properties = properties
        common.gamepad = GamepadManager(properties["controller"])

        self.using_mouse=True
        self.installEventFilter(self)
        self.gamepad_signal.connect(self.hide_mouse)
        common.gamepad.register_any(self.gamepad_event)

        self.data_path = join_module_path("data.xml")

        self.apply_style(":/style/MainWindow.css")

        self.main_window = MainWindow()
        ex_code = self.exec()
        common.stop_event.set()
        sys.exit(ex_code)
        
    def hide_mouse(self):
        self.setOverrideCursor(Qt.BlankCursor)
        self.main_window.setCursor(Qt.BlankCursor)
        self.using_mouse=False

    def apply_style(self, path):
        file = QFile(path)
        file.open(QFile.ReadOnly)
        data = str(file.readAll())
        self.setStyleSheet(data)

    @lock("mouse_flag")
    def gamepad_event(self, type, code, state):
        if self.using_mouse:
            if type == "Absolute":
                if -0.5 < state/(1<<15) < 0.5:
                    return
            self.gamepad_signal.emit()

    @lock("mouse_flag")
    def mouse_event(self):
        self.setOverrideCursor(Qt.ArrowCursor)
        self.using_mouse=True
    
    def eventFilter(self, object, event):
        if event.type() == QEvent.MouseMove:
            if not self.using_mouse:
                self.mouse_event()
                self.mouse_signal.emit()
        return False